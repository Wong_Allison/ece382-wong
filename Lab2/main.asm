;**********************************************************************
;
; COPYRIGHT 2016 United States Air Force Academy All rights reserved.
;
; United States Air Force Academy     __  _______ ___    _________
; Dept of Electrical &               / / / / ___//   |  / ____/   |
; Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
; 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
; USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
;
; ----------------------------------------------------------------------
;
;   FILENAME      : main.asm
;   AUTHOR(S)     : C2C Allison Wong
;   DATE          : 25 September 2018
;   COURSE		  : ECE 382
;
;   Lab 2 Assembly Code
;
;   DESCRIPTION   : Code to control a 595 shift register. Shifts through 4 LEDs in HW, and debounces the MSP430 button.
;
;   DOCUMENTATION : None
;
; Academic Integrity Statement: I certify that, while others may have
; assisted me in brain storming, debugging and validating this program,
; the program itself is my own work. I understand that submitting code
; which is the work of other individuals is a violation of the honor
; code.  I also understand that if I knowingly give my original work to
; another individual is also a violation of the honor code.
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			; Setup P1.3 button for input
			bic.b	#BIT3, &P1DIR	;P1.3 is input
			bis.b	#BIT3, &P1REN	;resistor enabled
			bis.b	#BIT3, &P1OUT	;resistor is pull-up

			; Setup P1.4 for output (SER)
			bis.b	#BIT4, &P1DIR	;P1.4 is output
			bis.b	#BIT4, &P1OUT	;SER = 1

			; Setup P1.5 for output (SRCLK)
			bis.b	#BIT5, &P1DIR	;P1.5 is output
			bic.b	#BIT5, &P1OUT	;SRCLK = 0

			; Setup P1.6 for output (RCLK)
			bis.b	#BIT6, &P1DIR	;P1.6 is output
			bic.b	#BIT6, &P1OUT	;RCLK = 0

			mov.w	#0, r4		;r4 = 0 to start with

loop:
			cmp		#0, r4
			jz		loop_cont		;only first run, r4 = 0, SER = 1

			cmp		#4, r4			;shift counter needs to be reset?
			jz		reset_count		;if yes, reset to SER = 1, r4 = 0

			bic.b	#BIT4, &P1OUT	;if no, SER = 0, continue
			jmp		loop_cont

reset_count:
			mov.w	#0x0000, r4		;r4 = 0, restart counting to 4
			bis.b	#BIT4, &P1OUT	;SER = 1, to be shifted in next

			jmp 	loop_cont		;continue, to wait for button

loop_cont:
			bit.b	#BIT3, &P1IN	;has button been pushed
			jnz		loop_cont		;if no(z still 1), keep checking

			;Button pushed
			call 	#btn_debounce	;debounce
			jmp		check_release	;jmp to check for button release

check_release:
			bit.b	#BIT3, &P1IN	;has button been released
			jz		check_release	;if no(z still 0), keep checking

			;Button released
			call	#btn_debounce	;debounce
			jmp		shift_reg		;if yes, shift register

btn_debounce:
			push 	r5
			mov.w	#0xD32, r5
			;Implement delay by decrementing and looping 0xD32 times
delay:		dec		r5
			jnz		delay
			pop		r5
			ret

shift_reg:
			bis.b	#BIT5, &P1OUT	;toggle SRCLK
			bic.b	#BIT5, &P1OUT

			inc		r4				;increment shift counter

			bis.b	#BIT6, &P1OUT	;toggle RCLK
			bic.b	#BIT6, &P1OUT

			jmp loop

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
