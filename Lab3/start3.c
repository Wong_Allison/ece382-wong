/**********************************************************************

 COPYRIGHT 2016 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : start3.c
   AUTHOR(S)     : C2C Allison Wong
   DATE          : 7 November 2018
   COURSE        : ECE 382

   Lab 3 - Interrupts - "Remote Control Decoding"

   DESCRIPTION   : This code utilizes a IR sensor to receive data from a remote control,
                   Timer interrupts and the general purpose pin are used to decode the
                   remote control.

   DOCUMENTATION : C2C Diamond explained to me how to shift the bits in Code Composer Studio
                   in order to read in the IR data.

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>

// delay code written in assembly
// make sure to drag/drop the assembly code too
extern void Delay160ms(void);
extern void Delay40ms(void);

// macros to make life easier
#define        IR_PIN            (P2IN & BIT6)
#define        HIGH_2_LOW        P2IES |= BIT6
#define        LOW_2_HIGH        P2IES &= ~BIT6

// Globals are bad, but these are constants and can't be changed accidently.
// Set them up for your remote.
const uint32_t Button1 = 0x20DF2AD5;
const uint32_t Button2 = 0x20DFBA45;
const uint32_t Button3 = 0x20DF3AC5;
const uint32_t Button4 = 0x20DF9A65;
const uint32_t Button5 = 0x20DFB847;

uint16_t  i = 0; // Counter counts how many times bits are shifted
uint32_t data = 0x00000000; // Stores data from IR

// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void main(void){

    WDTCTL=WDTPW+WDTHOLD;  // stop WDT

    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;


    P2SEL &= ~BIT6;    // P2.6SEL = 0       // Set up P2.6 as GPIO not XIN as before
    P2SEL2 &= ~BIT6;   // P2.6SEL2 = 0      // This action takes three lines of code.
    P2DIR &= ~BIT6;    // P2.6 is output

    P2IFG &= ~BIT6;     // Clear any interrupt flag on P2.6
    P2IE  |= BIT6;      // Enable P2.6 interrupt

    HIGH_2_LOW;         // Check the header out.  P2IES changed.

    P1DIR |= BIT0|BIT6;     // Set LEDs as outputs
    P1OUT &= ~(BIT0|BIT6);  // And turn the LEDs off

    P1DIR |= BIT4|BIT5;     // Set P1.4 and P1.5 to outputs
    P1OUT &= ~(BIT4|BIT5);  // Turn P1.4 and P1.5 off

    TA0CCR0 = 16000;        // Create a 16ms roll-over period
    TA0CTL &= ~TAIFG;       // Clear flag before enabling interrupts = good practice
    TA0CTL |= ID_3 | TASSEL_2 | MC_1; // Use 1:8 prescalar off SMCLK and enable interrupts

    __enable_interrupt(); // Enable interrupts

    while(1){
        if(i == 32){ //If the data bits shifted 32 times, LED logic

            // Toggle the Green led on for 1 second
            P1OUT |= BIT6;
            __delay_cycles(8000000);
            P1OUT &= ~BIT6;

            if(data == Button1){
                P1OUT |= BIT4; // Button 1: Turn P1.4 LED on
            }
            else if(data == Button2){
                P1OUT &= ~BIT4; // Button 2: Turn P1.4 LED off
            }
            else if(data == Button3){ // Button 3: P1.4 LED blinks 3 times
                __delay_cycles(8000000);//1
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
                __delay_cycles(8000000);//2
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
                __delay_cycles(8000000);//3
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
            }
            else if(data == Button4){ // Button 4: P1.4 LED blinks 5 times
                __delay_cycles(8000000); //1
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
                __delay_cycles(8000000); //2
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
                __delay_cycles(8000000); //3
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
                __delay_cycles(8000000); //4
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
                __delay_cycles(8000000); //5
                P1OUT |= BIT4;
                __delay_cycles(8000000);
                P1OUT &= ~BIT4;
            }
            else if(data == Button5){ // Button 5: both P1.4/P1.5 LEDs turn on/off
                __delay_cycles(80000000);
                P1OUT |= BIT4|BIT5;
                __delay_cycles(80000000);
                P1OUT &= ~(BIT4|BIT5);
            }
            // Reset shift count and IR data
            i = 0;
            data = 0x00000000;
        }
    } // end infinite loop
} // end main



// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR
__interrupt void pinChange (void) {
    uint8_t    pin;
    uint16_t   pulseDuration;          // The timer is 16-bits

    P1OUT ^= BIT0; //Toggle red LED

    if (IR_PIN)  // is pin high or low?
        pin=1;
    else
        pin=0;

    switch (pin) {
        case 0: // !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
            pulseDuration = TA0R;

            if(1300 < pulseDuration && pulseDuration < 2200){ // if 1-bit
                i = i + 1;          // Increment counter
                data = data * 2;    // Shift bits
                data = data + 1;    // Shift a 1 in

            }
            else if(200 < pulseDuration && pulseDuration < 800){ // if 0-bit
                i = i + 1;          // Increment counter
                data = data * 2;    // Shift bits, shift 0 in
            }
            P2IFG &= ~BIT6;         // Clear interrupt flag

            LOW_2_HIGH;             // Set up pin interrupt on positive edge
            break;

        case 1: // !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
            TA0R = 0x0000;          // time measurements are based at time 0
            TA0CTL |= MC_1 | TAIE;
            HIGH_2_LOW;             // Set up pin interrupt on falling edge
            break;
    } // end switch

    P2IFG &= ~BIT6;                // Clear the interrupt flag to prevent
                                   // immediate ISR re-entry

} // end pinChange ISR


#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflow (void) {
    TA0CTL &= ~MC_1;              // Turn off Timer A and Enable Interrupt
    TA0CTL &= ~TAIE;
    TA0CTL &= ~TAIFG;  // clear timer a interrupt
}
