;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
DATA: .byte 0x22, 0x33, 0x11, 0x14

            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

start:
		mov.b 	@DATA, 0(r6)
		cmp r6, 0x11
		jz push1
		jmp next1

push1:	push byte 0xC0
		jmp next1

next1: 	mov.b @DATA, 2(r7)
		cmp r7, 0x22
		jz push2
		jmp	next2

push2:	push 0xDFEC
		jmp next2

next2: 	mov.b @DATA, 4(r8)
		cmp r8, 0x33
		jz push3
		jmp next3

push3:	push byte 0x15
		jmp next3

next3: 	mov.b @DATA, 6(r9)
		cmp r9, 0x44
		jz push4
		jmp next4

push4: 	push 1
		jmp next4

next4: 	pop r9
		pop.b r8
		pop.b r7
		pop r6

		jmp forever

		forever:
			jmp forever

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
