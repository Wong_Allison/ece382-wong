ECE382 - Lab 1
=================
C2C Allison Wong
----------------

Prelab Flowchart
----------------
![](images/flowchart.JPG "")

Prelab Questions
----------------
1.	What should your program do if a value is out of range?

    a.	If the value is out of range, the program should let the user know that the input is invalid and cannot be processed.
    
2.	What should your program do if an operator is unknown?

    a.	If an operator is unknown, zero will be stored in the next byte in RAM, the memory pointer will be incremented, the second operand will become the initial value, and the process will restore running at “store next byte as operation.”
    
3.	Are there any bytes that you cannot use as an operand?
    
    a. Yes, the operands can only be positive integers, from 0 to 255. When written in binary, the eighth bit cannot be used.
    	
4.	What happens if you do not find a 0x55 (END_OP) command?
    
    a.	If you do not find the END_OP command, the program will continue to loop endlessly, and it will run out of resources eventually.
    
Documentation Statement
=======================
None.