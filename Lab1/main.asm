; Name: C2C Allison Wong
; Section: M3A
; Lab 1 - A Simple Calculator
; Documentation Statement: None
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;myProgram:	.byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55 ;Test Case 1
;myProgram:	.byte	0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55 ;Test Case 2
;myProgram:	.byte	0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
myProgram: .byte	0xFF, 0x33, 0xFF, 0x44, 0xEE, 0x22, 0xFF, 0x33, 0xFF, 0x55

ADD_OP		.equ	0x11	; Addition operation
SUB_OP		.equ	0x22	; Subtraction operation
MUL_OP		.equ	0x33	; Multiplication operation
CLR_OP		.equ	0x44	; Clear operation
END_OP		.equ	0x55	; End operation
MAX_NUM		.equ	255		; Maximum integer value
MIN_NUM		.equ	0		; Minimum integer value

			.data
myResults:	.space 20		; Allot space in RAM for myResults


;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
main:
			mov.w		#myProgram, r4		; Initialize ROM pointer
			mov.w		#myResults, r5		; Initialize RAM pointer
			mov.b		@r4+, r6			; Store first operand in r6

continue:
			mov.b		@r4+, r7		; Store operation in r7
			cmp			#END_OP, r7		; Is operation/r7 END?
			jz			forever			; If so, end program
			mov.b		@r4+, r8		; Store the second operand in r8
			cmp			#CLR_OP, r7		; Is operation CLR?
			jz			Clear_Op		; If so, jump to Clear_Op
			cmp			#ADD_OP, r7		; Is operation ADD?
			jz			Addition_Op		; If so, jump to Addition_op
			cmp			#SUB_OP, r7		; Is operation SUB?
			jz			Subtraction_Op	; If so, jump to Subtraction_op
			cmp			#MUL_OP, r7		; Is operation MUL?
			jz			Multiply_Op		; If so, jump to Multiply_op
			jmp			Clear_Op		; Else, unknown operation, jump to Clear_op

Clear_Op:
			mov.b 		r8, r6			; Second operand becomes first operand for next operation
			mov.b 		#0, 0(r5)		; Store zero in RAM/myResults
			inc 		r5				; Increment RAM pointer
			jmp			continue

Addition_Op:
			add.w		r8, r6			; Add two operands, r6 and r8, store result as the next first operand, r6
			jmp			Record_Result

Subtraction_Op:
			sub.w		r8, r6			; Subtract two operands, r6 and r8, store result as next first operand, r6
			jmp			Record_Result

Multiply_Op:
			cmp			#0, r6			; If first operand zero, jump to Multiply_Zero
			jz			Multiply_Zero
			cmp			#0, r8			; If second operand zero, jump to Multiply_Zero
			jz			Multiply_Zero
			mov.b		r6, r9			; Loop counter, r9
			clr			r6				; Clear r6 to keep intermediate results in it
			add.w		r8, r6			; First calculation
			jmp 		Add_Loop

Add_Loop:
			dec			r9				; Decrement r9, the number of times remaining to add
			jz			Record_Result	; When r9 reaches zero, stop adding
			add.w		r8, r6			; Running sum in r6
			jmp			Add_Loop		; Keep adding until r9 is zero


Multiply_Zero:
			mov.b		#0, r6			; Answer is 0
			jmp			Record_Result

Record_Result:
			cmp 		#MAX_NUM+1, r6	; Is result greater than MAX_NUM?
			jge			Exceed_Max		; If so, jmp to Exceed_Max
			cmp			#MIN_NUM, r6	; Is result less than MIN_NUM?
			jl			Below_Min		; If so, jmp to Below_Min

Continue_Record:
			mov.b		r6, 0(r5)		; Store result in RAM/myResults
			inc			r5				; Increment RAM/myResults pointer
			jmp			continue

Exceed_Max:
			mov.b		#MAX_NUM, r6	; Set result to MAX_NUM
			jmp			Continue_Record	; Record MAX_NUM, 255, as result

Below_Min:
			mov.b		#MIN_NUM, r6	; Set result to MIN_NUM
			jmp			Continue_Record	; Record MIN_NUM, 0, as result

forever: 	jmp forever
                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
