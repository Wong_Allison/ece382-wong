/**********************************************************************

 COPYRIGHT 2016 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : uart.c
   AUTHOR(S)     : C2C Allison Wong
   DATE          : 12/3/2018
   COURSE        : ECE 382

   Lesson/Hw/...e.g., Assignment 7

   DESCRIPTION   : This code simply provides a template for all
                   C assignments to use.
                     - Be sure to include your *Documentation*
                       Statement below!

   DOCUMENTATION : Serial Comm Slides

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <msp430.h>
#include "uart.h"

void uart_setup()
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    P1SEL |= BIT2;
    P1SEL2 |= BIT2;
    UCA0CTL1 = UCSWRST;
    UCA0CTL1 |= UCSSEL_2;

    UCA0CTL0 = 0;
    UCA0BR0 = 104;
    UCA0BR1 = 0;
    UCA0MCTL = UCBRS0;
    UCA0CTL1 &= ~UCSWRST;
}


void uart_putc(const int8_t c){
    while(!(IFG2 & UCA0TXIFG));

    UCA0TXBUF = c;

}

void uart_putString(const int8_t *string, uint8_t len){
    int i = 0;
    for(i=0; i<len; i++)
    {
        uart_putc(string[i]);
    }

}


