/**********************************************************************

 COPYRIGHT 2016 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : pwm.c
   AUTHOR(S)     : C2C Allison Wong
   DATE          : 12/3/2018
   COURSE        : ECE 382

   Lesson/Hw/...e.g., Assignment 7

   DESCRIPTION   : This code simply provides a template for all
                   C assignments to use.
                     - Be sure to include your *Documentation*
                       Statement below!

   DOCUMENTATION : None

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/
#include <msp430.h>
#include <stdint.h>
#include "pwm.h"

void pwm_setup(uint16_t periodCycles, uint16_t initialWidthCycles)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    P2DIR |= BIT1;
    P2SEL |= BIT1;
    TA1CTL &= ~MC1|MC0;
    TA1CTL |= TACLR;
    TA1CTL |= TASSEL1;
    TA1CCR0 = periodCycles;
    TA1CCR1 = initialWidthCycles;
    TA1CCTL1 |= OUTMOD_7;
    TA1CTL |= MC0;
}

void pwm_pulse_width(uint16_t widthCycles) // Update TA1CCR1 to width
{
    TA1CCR1 = widthCycles;
}

void pwm_period(uint16_t periodCycles)  // Update TA1CCR0 to period
{
    TA1CCR0 = periodCycles;
}
