/**********************************************************************

 COPYRIGHT 2016 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : main.c
   AUTHOR(S)     : C2C Allison Wong
   DATE          : 12/3/2018
   COURSE        : ECE 382

   Lesson/Hw/...e.g., Assignment 7

   DESCRIPTION   : This code simply provides a template for all
                   C assignments to use.
                     - Be sure to include your *Documentation*
                       Statement below!

   DOCUMENTATION : C2C Slatkavitz explained to me the relationship between the required functions.
                   C2C Diamond pointed out to me why my LCD was not working right.
                   C2C Yi explained to me the math behind the pwm function.
                   Serial Comm slides for itoa function.

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/
#include <msp430.h>
#include <stdint.h>
#include <stdbool.h>
#include "servo.h"
#include "lcd.h"
#include "pwm.h"
#include "adc.h"

int main(void)
{
    lcd_init();  // Initiate LCD
    pwm_setup(20000, 1400);  // Set up PWM and timers
    adc_init();  // Set up ADC
    lcd_initiate_intro();  // Display splash screen

    while(1)
    {
        char angle[3];
        itoa(read_adc()*.20, angle, 10);  // Read angle from ADC and turn into string to display

        if (read_adc() < 10){  // If angle value length is 1
            lcd_write_string(angle, 1);
        }
        else if (read_adc() >= 100){  // If angle value length is 3
            lcd_write_string(angle, 3);
        }
        else{
            lcd_write_string(angle, 2);  // If angle value length is 2
        }

        __delay_cycles(1000000);  //Delay before clearing and refreshing
        lcd_clear();  // Clear LCD
        lcd_cursor_position(0x00);
        uint16_t pulseWidth = (500+1.9*read_adc());
        servo_pulse_width(pulseWidth);
    }
    return 0;
}

/**
Inter to asci
value: integer to convert, result: buffer to put the characters into, base: 10 if decimal
*/
void itoa(int16_t value, char* result, uint16_t base){
      // check that the base if valid
      if (base < 2 || base > 36) { *result = '\0';}

      char* ptr = result, *ptr1 = result, tmp_char;
      int16_t tmp_value;

      do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
      } while ( value );

      *ptr-- = '\0';
      while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
      }
}


