/**********************************************************************

 COPYRIGHT 2016 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : lcd.c
   AUTHOR(S)     : C2C Allison Wong
   DATE          : 12/3/2018
   COURSE        : ECE 382

   Lesson/Hw/...e.g., Assignment 7

   DESCRIPTION   : This code simply provides a template for all
                   C assignments to use.
                     - Be sure to include your *Documentation*
                       Statement below!

   DOCUMENTATION : LCD datasheet.

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/
#include "uart.h"
#include "lcd.h"
#include <msp430.h>
#include <stdint.h>

void lcd_init()  // Set up UART port
{
    uart_setup();
}

void lcd_clear()  // Clears the LCD
{
    uart_putc(0xFE);
    uart_putc(0x01);
}

void lcd_cursorL()  // Move Cursor Left 1 Space
{
    uart_putc(0xFE);
    uart_putc(0x10);
}

void lcd_cursorR()  // Move Cursor Right 1 Space
{
    uart_putc(0xFE);
    uart_putc(0x14);
}

void lcd_backlightON()  // Backlight on
{
    uart_putc(0x7C);
    uart_putc(0x9D);
}

void lcd_backlightOFF()  // Backlight off
{
    uart_putc(0x7C);
    uart_putc(0x80);
}

void lcd_cursor_position(const int8_t pos)  // Set cursor position (0x80+n)
{
    uart_putc(0xFE);
    uart_putc(pos);
}

void lcd_write_char(const int8_t c)  // Call write char function in UART file
{
    uart_putc(c);
}

void lcd_write_string(const int8_t *string, uint8_t len)  // Call write string function
{
    uart_putString(string, len);
}

void lcd_initiate_intro()  // Display splash screen on power-up
{
    // Clear LCD and print the splash screen
    lcd_clear();
    lcd_cursor_position(0x80);
    lcd_write_string("ECE 382, Fall 2018", 18);
    lcd_cursor_position(0x93);
    lcd_write_string(" C2C Allison Wong", 17);
    __delay_cycles(3000000);
    lcd_clear();
}
