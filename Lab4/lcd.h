/**********************************************************************

 COPYRIGHT 2016 United States Air Force Academy All rights reserved.

 United States Air Force Academy     __  _______ ___    _________
 Dept of Electrical &               / / / / ___//   |  / ____/   |
 Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
 USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|

 ----------------------------------------------------------------------

   FILENAME      : lcd.h
   AUTHOR(S)     : C2C Allison Wong
   DATE          : 12/3/2018
   COURSE        : ECE 382

   Lesson/Hw/...e.g., Assignment 7

   DESCRIPTION   : This code simply provides a template for all
                   C assignments to use.
                     - Be sure to include your *Documentation*
                       Statement below!

   DOCUMENTATION : None

 Academic Integrity Statement: I certify that, while others may have
 assisted me in brain storming, debugging and validating this program,
 the program itself is my own work. I understand that submitting code
 which is the work of other individuals is a violation of the honor
 code.  I also understand that if I knowingly give my original work to
 another individual is also a violation of the honor code.

 **********************************************************************/

#ifndef SERIAL_H_
#define SERIAL_H_

#include <stdint.h>

void lcd_init();
void lcd_clear();
void lcd_cursorL();
void lcd_cursorR();
void lcd_backlightON();
void lcd_backlightOFF();
void lcd_cursor_position(const int8_t pos);
void lcd_write_char(const int8_t c);
void lcd_write_string(const int8_t *string, uint8_t len);
void lcd_initiate_intro();

#endif /* SERIAL_H_ */
